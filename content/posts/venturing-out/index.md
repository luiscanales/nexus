---
title: "Venturing Out"
date: 2021-03-05T18:32:08-07:00
draft: false
tags: ["bootstrap"]
---

Like many developers, I've been toying around with different ideas for a software product business for a long time. I think it's time for a real push towards a small product to get the gears turning on everything regarding a software business: marketing, customer support, and sales.

I hope to use Hey World here to build the product in public. Here's my first attempt:

Shopify App
Sparked by Preetam Nath's  Guide to building profitable Shopify apps, I decided to take my first attempt by starting small on an already established platform. 

I'll have more details as soon as I have something to show.
