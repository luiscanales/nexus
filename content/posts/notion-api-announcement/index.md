---
title: "Notion API Announcement"
date: 2021-05-13T10:04:10-07:00
draft: false
summaryImage: "cover.png"
tags: ["wevdev","notion"]
---

[Notion has just announced their public BETA API!](https://twitter.com/NotionAPI/status/1392884082560753670)

This is awesome. I use notion as my main mind -> computer tool. It's the first tool I've ever been extremely happy with and stuck to... I suppose it's due to its versatility and its place in my mind between markdown and html formatting.

Some thoughts on what I'm thinking of building:

## Blog sync
This site currently uses this process to update pages:
1. Create a markdown file on my workstation 
2. Edit
3. Git commit & push to [Gitlab](https://gitlab.com/luiscanales/nexus)
4. DigitalOcean App platform automatically detects the changes and auto builds the new version of the site.

While already pretty straightforward, it does require me to go outside of Notion (where most of my notes are) rather than just have a section where it more or less mirrors the site. Or at least, the content.


## AirTable integrations
I frequently use Notion to make tables but I'd really love to be able to not only use the full power of Airtable, but use better filtering and share only my table (and not my notes surrounding my table). 


## External Integrations
The notion team already mentions integrations with Zapier, which is awesome and opens up tons of further integrations down the line. Truth be told, I'm still exploring those possibilities but when I do thing of specific integrations, I'll make sure to update this page.

