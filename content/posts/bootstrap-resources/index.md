---
title: "Bootstrap Resources"
date: 2021-04-21T05:08:33-07:00
draft: false
ShowToc: true
tags: ["bootstrap", "list","resources"]
---

These are some of the resources I'm using on my bootstrapped journey. It really is a potluck of podcasts, books, newsletters, and courses. On this page I'm planning to share some of what's helped me the most via useful tips, inspiration, or general mindset on the bootstrapped approach to starting a software company. 

# Podcasts
## IndieHackers 
The [IndieHackers](https://www.indiehackers.com/podcasts) site and podcasts are some of the best resources I've stumbled upon. They are a mix of a community, inspiration, and tactics you can incoporate onto your bootstrapped journey.

Some favorites examples:
1. [Every Indie Hacker Has an Online Course In Them](https://www.indiehackers.com/podcast/200-ab-mp-aa) - Explores the new direction online education is taking and how some of the best businesses indiehackers can start when you have no idea on what to do is to start teaching people.
2. [Bootstrapping from an Investor's Point of View](https://www.indiehackers.com/podcast/163-rob-walling-of-tinyseed) by Rob Walling kind of kicked off my recent wave of really giving entrepreneurship a real shot this time around. He talks about his approach going from working a job to spinning off smallish products until he could buy his time back. After buying back your time you can more or less pick your own path - including building a SaaS. [Lesson's Learned](https://www.indiehackers.com/podcast/082-rob-walling-of-drip-and-tinyseed) is another good episode with Rob. He also has an awesome podcast (listed below).
3. [Entering a Market with No Domain Expertise](https://www.indiehackers.com/podcast/194-kevin-lee-of-immi) with Kevin Lee gives his backstory on starting a Noodle company with no background experience. It illuminates how much luck, skill, and tenacity is involved in really breaking through. Kevin's story also shows how putting a piece of yourself and your identity in what your building is important to keep pushing through the hard days of your journey.

## Everyone Hates Marketers
[Everyone Hates Marketers](https://www.everyonehatesmarketers.com/) is an great resource on learning better marketing that doesn't make you hater yourself at night. His newsletter starts with 8 lessons of learning how to **Stand the Fuck Out** and get rid of of your self limiting beliefs, amongst other things.

Some of my favorites: 
1. [How to Stop Marketing with Your Ego and Become and Effective Marketer](https://www.everyonehatesmarketers.com/podcast/stop-marketing-ego) is an amazing insight into a seasoned marketer's mindset. Ed Nevraumont goes over how he establishes and (more importantly) measures his marketing funnels and their effectiveness.
2. [Please, I Beg You, Don't Try to "Create a New Category"](https://podcasts.apple.com/us/podcast/please-i-beg-you-dont-try-to-create-a-new-category/id1221256195?i=1000514054325) argues why you have such an uphill battle when you try to create a new product category; and how few things really are "NEW" vs focusing on niche people, markets and products.

## Startups for the Rest of Us
Rob Walling does a mix of inspirational, strategy, and tactical tips in his [Startups for the Rest of Us](https://www.startupsfortherestofus.com/) postcast. It's one of my favorites to listen to. I recommend just starting with his [Greatest Hits](https://www.startupsfortherestofus.com/category/greatest-hits) and going from there.

# Books
## Show Your Work! - Austin Kleon
[Show Your Work!: 10 Ways to Share Your Creativity and Get Discovered](https://www.amazon.com/dp/076117897X/ref=cm_sw_em_r_mt_dp_CC5TAX9VPR5HDWBJHPP3) is part of the inspiration to finally start submitting posts on a regular basis here. It echoes a lot of the comments I've heard from across other communities and books : Take a sharing first approach, build in public, and be transparent about what you know.

## Start Small Stay Small - Rob Walling
Rob makes another entry here in his [Start Small Stay Small](https://www.amazon.com/dp/B003YH9MMI/ref=cm_sw_r_tw_dp_9D6MW8V8T2TRAF9Y3PVF) book. While a lot of the tactics are pretty dead or antiquated (corrected I hope by taking specific courses), the general mindset is still useful when it comes to starting from scratch. Rob has mentioned that he plans to updated the book, and when he does I'll take a read and update the entry.


# Articles
This section could be massive but I'll just focus on some of the articles that really helped me out with practical examples.
## How to E-Bomb
[How to E-Bomb](https://stackingthebricks.com/the-ebomb-recipe-that-works/) is a great read on how to create small 'ebombs' or *' "actionable educational content marketing"'*. It introduced me into the whole concept of giving as much as you can if you want to make friends.

## Micro-SaaS Guide: Build a Profitable Business in 2021
[Micro-SaaS Guide: Build a Profitable Business in 2021](https://www.preetamnath.com/micro-saas) is a fantastic deep dive with practical tips and strategies on tackling a profitable Micro-SaaS in 2021.

## Why more developers should start content businesses
[Why more developers should start content businesses](https://rocketgems.com/blog/developer-content-businesses/) is what really pushed me into actually creating some content (like what you're reading right now). I plan to throw up a guide on how I made a Turbo Rails shopify application instead of using React. 

# Courses
## The Complete Digital Marketing Course - 12 courses in 1
I'm currently making my way through [this practical marketing course](https://www.udemy.com/course/learn-digital-marketing-course/) and I already am enjoying it. It's filled with practical advice around verifying a market, tactics on HOW to do that, etc. I'll update this entry once I'm halfway through it.