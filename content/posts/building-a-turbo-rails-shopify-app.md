---
title: "How I built a Rails Shopify App with Turbo"
date: 2021-04-24T23:35:50-07:00
draft: false
cover: "/posts/building-a-turbo-rails-shopify-app/cover.png"
toc: true
tags: ["turbo","rails","howto","shopify","webdev"]
---

As of this writing I'm not finding many great resources on how to build a Shopify app using Turbo. This was right as the transition from Turbolinks to Turbo is in full swing, and their current "[how to](https://shopify.dev/tutorials/authenticate-server-side-rendered-embedded-apps-using-rails-and-turbolinks)" guide is close but missed the mark on certain things. So this is my attempt at bolstering that guide.

# Getting Started
You'll need these installed:
1. [Rails](https://rubyonrails.org/)
2. [Shopify CLI](https://shopify.dev/tools/cli/installation)
3. [ngrok](https://ngrok.com/download)
4. [Shopify Parner Account](https://www.shopify.com/partners) (Note: You'll need these to create an app in the first place.)
5. [Shopify Development Store](https://help.shopify.com/en/partners/dashboard/managing-stores/development-stores) (Sample store to play with.)

Everything else we'll walk through. Yes, ngrok is needed due to the fact that embedded Shopify applications need to callback to a real resolvable hostname, not localhost.

# Initiating Rails Application
Let's start by initializing a standard (limited to `6.0.3` for now) rails application:

{{< highlight bash >}}
rails new $APPNAME
cd $APPNAME
bundle add dotenv-rails shopify_app
rails g shopify_app
{{< / highlight >}}

Line #3 is needed to pass along environment variables.
Modify your server (if you're using an IDE) so it listens on port 8081.

Add this to your root file `.shopify-cli.yml`
## `.shopify-cli.yml`
{{< highlight yaml >}}
---
project_type: rails
organization_id: $YOUR_ORG_ID
{{< / highlight >}}

Next, start up your Shopify cli application so it can do all the heavy lifting of registering your application, entering your callback URL, generating the API keys and inserting them into a `.env` file, etc.

{{< highlight bash >}}
shopify connect
shopify serve
{{< / highlight >}}

Then exit out of it. Again, this is mainly so that the app does the heavy configuration on the shopify side. Ensure the tunnel stays on by visiting the URL it spat out in the beginning. 

Ensure you add scopes to initializers since this hook seems to be missing from their currently generated file

## `config/initializers/shopify_app.rb`
{{< highlight ruby>}}
config.scope = ENV.fetch('SCOPES','').presence 
# Consult this page for more scope options:
{{< /highlight >}}

Let's bring up the application and ensure you can add the page to your existing developer shop.
{{< highlight bash>}}
rails db:reset
rails db:migrate
shopify serve
{{< /highlight >}}

After this point you should be able to start your Rails app through your IDE or however you normally would. It will be accessible through the ngrok tunnel.

# Wiring in Turbo
Add the [Turbo gem](https://github.com/hotwired/turbo-rails) and node modules

{{< highlight bash >}}
bundle add turbo-rails
rails turbo:install
yarn add @shopify/polaris @shopify/app-bridge-utils @shopify/app-bridge
{{< /highlight >}}

Add now to add the secret sauce to shopify_app.js:
## `app/javascript/shopify_app/shopify_app.js`
{{< highlight javascript "linenos=table,hl_lines=8" >}}
import { Turbo } from "@hotwired/turbo-rails";
import { getSessionToken } from "@shopify/app-bridge-utils";

const SESSION_TOKEN_REFRESH_INTERVAL = 2000; // Request a new token every 2s

document.addEventListener('DOMContentLoaded', async () => {
  const data = document.getElementById('shopify-app-init').dataset;
  const AppBridge = window['app-bridge'];
  const createApp = AppBridge.default;
  window.app = createApp({
    apiKey: data.apiKey,
    shopOrigin: data.shopOrigin,
  });

  // Wait for a session token before trying to load an authenticated page
  await retrieveToken(app);

  // Keep retrieving a session token periodically
  keepRetrievingToken(app);

  // Redirect to the requested page
  Turbo.visit(data.loadPath);
});

async function retrieveToken(app) {
  window.sessionToken = await getSessionToken(app);
}

function keepRetrievingToken(app) {
  setInterval(() => {
    retrieveToken(app);
  }, SESSION_TOKEN_REFRESH_INTERVAL);
}


// Intercept every Turbo request and load Shopify session token
document.addEventListener("turbo:before-fetch-request", function (event) {
  let headers = event.detail.fetchOptions.headers;
  const token = window.sessionToken;
  headers["Authorization"] = `Bearer ${token}`;
});

// Force redirect via turbo using turbo_redirect_to helper in controller.
// Mandatory for Safari since it's losing JWT token during 302 redirect.
document.addEventListener("turbo:before-fetch-response", (event) => {
  const response = event.detail.fetchResponse;
  const status = response.statusCode;
  const location = response.header("Location");

  if (status === 300 && location !== null) {
    event.preventDefault();
    Turbo.visit(location);
  }
});
{{< /highlight >}}

Again, refer to the linked [Turbolinks Rails App](https://shopify.dev/tutorials/authenticate-server-side-rendered-embedded-apps-using-rails-and-turbolinks) for a full explanation of the above code. Some specific call outs

## Initialization 
These lines grap the shopify bridge object that contains the key and session token we need to interact with the Shopify API.
{{< highlight javascript>}}
  document.addEventListener('DOMContentLoaded', async () => {
  const data = document.getElementById('shopify-app-init').dataset;
  const AppBridge = window['app-bridge'];
  const createApp = AppBridge.default;
  window.app = createApp({
    apiKey: data.apiKey,
    shopOrigin: data.shopOrigin,
  });
{{< /highlight >}}

## Inject session token before every fetch
This section contains what is arguably the heavy lifting Turbo does to make our regular multi-page application play nice with the session tokens that Shopify requires:
{{< highlight javascript>}}
// Intercept every Turbo request and load Shopify session token
document.addEventListener("turbo:before-fetch-request", function (event) {
  let headers = event.detail.fetchOptions.headers;
  const token = window.sessionToken;
  headers["Authorization"] = `Bearer ${token}`;
});
{{< /highlight >}}

Congrats. Now you have turbo wired in. Let's ensure this generating a sample page that pulls products.

# Sample Products Page
Generate controller:
{{< highlight bash >}}
rails generate controller splash_page index
{{< /highlight >}}

Keep the landing page simple since the products Page will get its own view:

## `app/controllers/SplashController.rb`
{{< highlight ruby>}}
class SplashPageController < ApplicationController
  include ShopifyApp::EmbeddedApp
  include ShopifyApp::RequireKnownShop
  include ShopifyApp::ShopAccessScopesVerification

  def index
    @shop_origin = current_shopify_domain
  end
end
{{< /highlight >}}

## `app/controllers/HomeController.rb`
{{< highlight ruby>}}
# frozen_string_literal: true

class HomeController < AuthenticatedController
  def index
    @shop_origin = current_shopify_domain
  end
end
{{< /highlight >}}

You'll need an unauthenticated splash page to load the session token first:
## `splash-page/index.html.erb`
{{< highlight html>}}
<style>
    .spinner {
        height: 100vh;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .spinner > div {
        width: 18px;
        height: 18px;
        background-color: #c4cdd5;
        border-radius: 100%;
        display: inline-block;
        -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
        animation: sk-bouncedelay 1.4s infinite ease-in-out both;
    }

    .spinner .bounce1 {
        -webkit-animation-delay: -0.32s;
        animation-delay: -0.32s;
    }

    .spinner .bounce2 {
        -webkit-animation-delay: -0.16s;
        animation-delay: -0.16s;
    }

    @-webkit-keyframes sk-bouncedelay {
        0%, 80%, 100% { -webkit-transform: scale(0) }
        40% { -webkit-transform: scale(1.0) }
    }

    @keyframes sk-bouncedelay {
        0%, 80%, 100% {
            -webkit-transform: scale(0);
            transform: scale(0);
        } 40% {
              -webkit-transform: scale(1.0);
              transform: scale(1.0);
          }
    }
</style>

<div class="spinner">
  <div class="bounce1"></div>
  <div class="bounce2"></div>
  <div class="bounce3"></div>
</div>
{{< /highlight >}}

And then add the route:

## `config/routes.rb`
{{< highlight ruby >}}
Rails.application.routes.draw do
  root to: 'splash_page#index'
  get '/home' , to: 'home#index'
  get '/products', to: 'products#index'
  mount ShopifyApp::Engine, at: '/'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
{{< /highlight >}}

Shopify loads the embedded page, so add this section to prevent an infinite authentication loop:
## `app/views/layouts/embedded.html.erb`
{{< highlight ruby >}}
...
   <%= content_tag(:div, nil, id: 'shopify-app-init', data: {
     api_key: ShopifyApp.configuration.api_key,
     shop_origin: @shop_origin || (@current_shopify_session.domain if @current_shopify_session),
     load_path: params[:return_to] || home_path,
     ...
   } ) %>
...


IE

<%= content_tag(:div, nil, id: 'shopify-app-init', data: {
      api_key: ShopifyApp.configuration.api_key,
      shop_origin: @shop_origin || (@current_shopify_session.domain if @current_shopify_session),
      load_path: params[:return_to] || home_path,
      debug: Rails.env.development?
}) %>
{{< /highlight >}}

## Products Page
## `app/controllers/ProductsController.rb`
{{< highlight ruby >}}
class ProductsController < AuthenticatedController
    def index   
        @products = ShopifyAPI::Product.find(:all, params: { limit: 10 })
    end
end
{{< /highlight >}}

Add the erb view:
## `app/views/products/index.html.erb`
{{< highlight html>}}
<%= link_to 'Back', home_path %>

   <h2>Products</h2>

   <ul>
     <% @products.each do |product| %>
       <li><%= link_to product.title, "https://#{@current_shopify_session.domain}/admin/products/#{product.id}", target: "_top" %></li>
     <% end %>
   </ul>
{{< /highlight >}}

and if you want a sample home page code:
{{< highlight html>}}
<h1>Home Page</h1>
<%= link_to 'Products', products_path(shop: @shop_origin) %>
<%= link_to 'Bare Products', products_path %>
{{< /highlight >}}

Congrats, you now have a turbo powered rails app that is pushing out the merchant's products to your home page.