---
title: "Shopify App Journey"
date: 2021-04-15T18:36:49-07:00
draft: false
summaryImage: "cover.png"
description: Step 1 of ???
tags: ["turbo","rails","shopify"]
---

I said I'd post something once I have something to show...Well, I suppose I do now. Although it's probably not what you think.

First Shopify Application Attempt

From Mar 5-Apr 6 , I attempted to build a Shopify application that deals in bundling products that a merchant would deem relevant and have the ability to discount them if you bundled them together.


The first major roadblock I ran into was the fact that Shopify heavily implies that you should use React and GraphQL in any app: (From https://shopify.dev/tutorials)

{{< figure 
    src="rails-search.png" 
    title="Searching Rails tutorials in shopify"
    width="500" 
    align="center" >}}

I don't have anything personally against React or GraphQL, but they're not my usual "reach for that tech stack first" type of solution for a variety of reasons. I feel like they tend to complicate CRUD style applications and my personal background involves more traditional server side programming languages like PHP and Python and relation databases like MySQL.


I was hesitant but finally took the plunge when I realized the differences between their React Polaris components and their HTML/CSS counterparts


{{< figure 
    height="700"
    src="media-composite.png" 
    title="Comparison of React and HTML code" >}}


Polaris being their design system: https://polaris.shopify.com/

As you can see from the side by side comparison, with just a media card representation I was already looking at at least double the lines of code! So naturally, I went down the react route.

Pain by Sessions
The next pain point I ran into was that Shopify started enforcing session token authentication, presumably because the major browsers starting enforcing the blocking of third party cookies. However, the problem was that all the guides I could find had cookie authentication as their main auth method. I had to essentially cobble together 3-4 guides 

Shopify's announcement: 
https://shopify.dev/changelog/all-embedded-apps-submitted-for-app-review-are-required-to-adopt-session-tokens

Guides I used on developing and understanding session tokens
https://www.christhefreelancer.com/build-a-shopify-app/
https://shopify.dev/tutorials/build-a-shopify-app-with-node-and-react
https://shopify.dev/tutorials/authenticate-your-app-using-session-tokens
https://shopify.dev/tutorials/build-rails-react-app-that-uses-app-bridge-authentication


In the next post I hope to outline a more step by step guide on how I eventually went down the
route of using Turbo + Session Auth + Rails to build my shopify application.